﻿using RestSharp;
using System;
using System.IO;
using System.ServiceProcess;
using System.Threading;


namespace EmailReports
{
    public partial class EmailReports : ServiceBase
    {
        public EmailReports()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            this.WriteToFile("Sudlife Email Report Service started {0}");
            this.ScheduleService();
        }

        protected override void OnStop()
        {
            this.WriteToFile("Sudlife Email Report Service stopped {0}");
            this.Schedular.Dispose();
        }

        private Timer Schedular;

        public void ScheduleService()
        {
            try
            {
                Schedular = new Timer(new TimerCallback(SchedularCallback));
                //  string mode = ConfigurationManager.AppSettings["Mode"].ToUpper();
                string mode = "DAILY";
                this.WriteToFile("Sudlife Email Report Service Mode: " + mode + " {0}");

                //Set the Default Time.
                DateTime scheduledTime = DateTime.MinValue;
                var x = "05:30 PM";

                if (mode == "DAILY")
                {
                    //Get the Scheduled Time from AppSettings.                   
                    scheduledTime = DateTime.Parse(x);
                    if (DateTime.Now > scheduledTime)
                    {
                        //If Scheduled Time is passed set Schedule for the next day.
                        scheduledTime = scheduledTime.AddDays(1);
                    }
                }

                if (mode.ToUpper() == "INTERVAL")
                {
                    //Get the Interval in Minutes from AppSettings.
                    int intervalMinutes = 2;

                    //Set the Scheduled Time by adding the Interval to Current Time.
                    scheduledTime = DateTime.Now.AddMinutes(intervalMinutes);
                    if (DateTime.Now > scheduledTime)
                    {
                        //If Scheduled Time is passed set Schedule for the next Interval.
                        scheduledTime = scheduledTime.AddMinutes(intervalMinutes);
                    }
                }

                TimeSpan timeSpan = scheduledTime.Subtract(DateTime.Now);
                string schedule = string.Format("{0} day(s) {1} hour(s) {2} minute(s) {3} seconds(s)", timeSpan.Days, timeSpan.Hours, timeSpan.Minutes, timeSpan.Seconds);
                this.WriteToFile("Sudlife Email Report Service scheduled to run after: " + schedule + " {0}");
                //Get the difference in Minutes between the Scheduled and Current Time.
                int dueTime = Convert.ToInt32(timeSpan.TotalMilliseconds);
                this.WriteToFile(dueTime.ToString());
                // WriteToFile("Actual Service Execution");
                ExecuteSudlifeServices();

                //Change the Timer's Due Time.
                Schedular.Change(dueTime, Timeout.Infinite);
            }
            catch (Exception ex)
            {
                WriteToFile("Sudlife Email Report Service Error on: {0} " + ex.Message + ex.StackTrace);

                //Stop the Windows Service.
                using (ServiceController serviceController = new System.ServiceProcess.ServiceController("SimpleService"))
                {
                    serviceController.Stop();
                }
            }
        }

        private void SchedularCallback(object e)
        {
            this.WriteToFile("Sudlife Email Report Service Log: {0}");
            this.ScheduleService();
        }

        private void WriteToFile(string text)
        {
            string path = "C:\\SudlifeMailer.txt";
            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format(text, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                writer.Close();
            }
        }

        public void ExecuteSudlifeServices()
        {
            WriteToFile("Sudlife Email Report service call hitted");
            var url = "http://api.sudlife.outwork.in:8500/admin/v1/emailreport/emailgeneration//";
            var client = new RestClient { BaseUrl = new Uri(url) };
            var request = new RestRequest { Method = Method.GET };
            var restResponse = (RestResponse)client.Execute(request);
            WriteToFile(restResponse.Content);
        }
    }
}
